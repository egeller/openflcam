#ifndef INCLUDED_ExtCam
#define INCLUDED_ExtCam

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS0(ExtCam)


class HXCPP_CLASS_ATTRIBUTES  ExtCam_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef ExtCam_obj OBJ_;
		ExtCam_obj();
		Void __construct();

	public:
		static hx::ObjectPtr< ExtCam_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~ExtCam_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("ExtCam"); }

		static int sampleMethod( int inputValue);
		static Dynamic sampleMethod_dyn();

		static Dynamic extcam_sample_method;
		static Dynamic &extcam_sample_method_dyn() { return extcam_sample_method;}
		static Dynamic testcam_dosomething;
		static Void init( );
		static Dynamic init_dyn();

		static ::String doSomething( ::String str);
		static Dynamic doSomething_dyn();

};


#endif /* INCLUDED_ExtCam */ 
