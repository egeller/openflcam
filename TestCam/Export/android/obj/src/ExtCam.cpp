#include <hxcpp.h>

#ifndef INCLUDED_ExtCam
#include <ExtCam.h>
#endif
#ifndef INCLUDED_cpp_Lib
#include <cpp/Lib.h>
#endif
#ifndef INCLUDED_openfl_utils_JNI
#include <openfl/utils/JNI.h>
#endif

Void ExtCam_obj::__construct()
{
	return null();
}

ExtCam_obj::~ExtCam_obj() { }

Dynamic ExtCam_obj::__CreateEmpty() { return  new ExtCam_obj; }
hx::ObjectPtr< ExtCam_obj > ExtCam_obj::__new()
{  hx::ObjectPtr< ExtCam_obj > result = new ExtCam_obj();
	result->__construct();
	return result;}

Dynamic ExtCam_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< ExtCam_obj > result = new ExtCam_obj();
	result->__construct();
	return result;}

int ExtCam_obj::sampleMethod( int inputValue){
	HX_STACK_PUSH("ExtCam::sampleMethod","ExtCam.hx",13);
	HX_STACK_ARG(inputValue,"inputValue");
	HX_STACK_LINE(13)
	return ::ExtCam_obj::extcam_sample_method(inputValue);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(ExtCam_obj,sampleMethod,return )

Dynamic ExtCam_obj::extcam_sample_method;

Dynamic ExtCam_obj::testcam_dosomething;

Void ExtCam_obj::init( ){
{
		HX_STACK_PUSH("ExtCam::init","ExtCam.hx",27);
		HX_STACK_LINE(28)
		if (((::ExtCam_obj::testcam_dosomething != null()))){
			HX_STACK_LINE(29)
			return null();
		}
		HX_STACK_LINE(30)
		::ExtCam_obj::testcam_dosomething = ::openfl::utils::JNI_obj::createStaticMethod(HX_CSTRING("testcam/TestCam"),HX_CSTRING("doSomething"),HX_CSTRING("(Ljava/lang/String;)Ljava/lang/String;"),null());
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(ExtCam_obj,init,(void))

::String ExtCam_obj::doSomething( ::String str){
	HX_STACK_PUSH("ExtCam::doSomething","ExtCam.hx",36);
	HX_STACK_ARG(str,"str");
	HX_STACK_LINE(37)
	::ExtCam_obj::init();
	HX_STACK_LINE(38)
	return ::ExtCam_obj::testcam_dosomething(str);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(ExtCam_obj,doSomething,return )


ExtCam_obj::ExtCam_obj()
{
}

void ExtCam_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(ExtCam);
	HX_MARK_END_CLASS();
}

void ExtCam_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic ExtCam_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"doSomething") ) { return doSomething_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"sampleMethod") ) { return sampleMethod_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"testcam_dosomething") ) { return testcam_dosomething; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"extcam_sample_method") ) { return extcam_sample_method; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic ExtCam_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 19:
		if (HX_FIELD_EQ(inName,"testcam_dosomething") ) { testcam_dosomething=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"extcam_sample_method") ) { extcam_sample_method=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void ExtCam_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("sampleMethod"),
	HX_CSTRING("extcam_sample_method"),
	HX_CSTRING("testcam_dosomething"),
	HX_CSTRING("init"),
	HX_CSTRING("doSomething"),
	String(null()) };

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(ExtCam_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(ExtCam_obj::extcam_sample_method,"extcam_sample_method");
	HX_MARK_MEMBER_NAME(ExtCam_obj::testcam_dosomething,"testcam_dosomething");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(ExtCam_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(ExtCam_obj::extcam_sample_method,"extcam_sample_method");
	HX_VISIT_MEMBER_NAME(ExtCam_obj::testcam_dosomething,"testcam_dosomething");
};

Class ExtCam_obj::__mClass;

void ExtCam_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("ExtCam"), hx::TCanCast< ExtCam_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void ExtCam_obj::__boot()
{
	extcam_sample_method= ::cpp::Lib_obj::load(HX_CSTRING("extcam"),HX_CSTRING("extcam_sample_method"),(int)1);
}

