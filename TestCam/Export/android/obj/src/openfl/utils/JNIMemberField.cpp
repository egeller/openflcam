#include <hxcpp.h>

#ifndef INCLUDED_flash_Lib
#include <flash/Lib.h>
#endif
#ifndef INCLUDED_openfl_utils_JNIMemberField
#include <openfl/utils/JNIMemberField.h>
#endif
namespace openfl{
namespace utils{

Void JNIMemberField_obj::__construct(Dynamic field)
{
HX_STACK_PUSH("JNIMemberField::new","openfl/utils/JNI.hx",114);
{
	HX_STACK_LINE(114)
	this->field = field;
}
;
	return null();
}

JNIMemberField_obj::~JNIMemberField_obj() { }

Dynamic JNIMemberField_obj::__CreateEmpty() { return  new JNIMemberField_obj; }
hx::ObjectPtr< JNIMemberField_obj > JNIMemberField_obj::__new(Dynamic field)
{  hx::ObjectPtr< JNIMemberField_obj > result = new JNIMemberField_obj();
	result->__construct(field);
	return result;}

Dynamic JNIMemberField_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JNIMemberField_obj > result = new JNIMemberField_obj();
	result->__construct(inArgs[0]);
	return result;}

Dynamic JNIMemberField_obj::set( Dynamic jobject,Dynamic value){
	HX_STACK_PUSH("JNIMemberField::set","openfl/utils/JNI.hx",128);
	HX_STACK_THIS(this);
	HX_STACK_ARG(jobject,"jobject");
	HX_STACK_ARG(value,"value");
	HX_STACK_LINE(130)
	::openfl::utils::JNIMemberField_obj::nme_jni_set_member(this->field,jobject,value);
	HX_STACK_LINE(131)
	return value;
}


HX_DEFINE_DYNAMIC_FUNC2(JNIMemberField_obj,set,return )

Dynamic JNIMemberField_obj::get( Dynamic jobject){
	HX_STACK_PUSH("JNIMemberField::get","openfl/utils/JNI.hx",121);
	HX_STACK_THIS(this);
	HX_STACK_ARG(jobject,"jobject");
	HX_STACK_LINE(121)
	return ::openfl::utils::JNIMemberField_obj::nme_jni_get_member(this->field,jobject);
}


HX_DEFINE_DYNAMIC_FUNC1(JNIMemberField_obj,get,return )

Dynamic JNIMemberField_obj::nme_jni_get_member;

Dynamic JNIMemberField_obj::nme_jni_set_member;


JNIMemberField_obj::JNIMemberField_obj()
{
}

void JNIMemberField_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(JNIMemberField);
	HX_MARK_MEMBER_NAME(field,"field");
	HX_MARK_END_CLASS();
}

void JNIMemberField_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(field,"field");
}

Dynamic JNIMemberField_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"set") ) { return set_dyn(); }
		if (HX_FIELD_EQ(inName,"get") ) { return get_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"field") ) { return field; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"nme_jni_get_member") ) { return nme_jni_get_member; }
		if (HX_FIELD_EQ(inName,"nme_jni_set_member") ) { return nme_jni_set_member; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JNIMemberField_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"field") ) { field=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"nme_jni_get_member") ) { nme_jni_get_member=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"nme_jni_set_member") ) { nme_jni_set_member=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JNIMemberField_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("field"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("nme_jni_get_member"),
	HX_CSTRING("nme_jni_set_member"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("set"),
	HX_CSTRING("get"),
	HX_CSTRING("field"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JNIMemberField_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(JNIMemberField_obj::nme_jni_get_member,"nme_jni_get_member");
	HX_MARK_MEMBER_NAME(JNIMemberField_obj::nme_jni_set_member,"nme_jni_set_member");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JNIMemberField_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(JNIMemberField_obj::nme_jni_get_member,"nme_jni_get_member");
	HX_VISIT_MEMBER_NAME(JNIMemberField_obj::nme_jni_set_member,"nme_jni_set_member");
};

Class JNIMemberField_obj::__mClass;

void JNIMemberField_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("openfl.utils.JNIMemberField"), hx::TCanCast< JNIMemberField_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void JNIMemberField_obj::__boot()
{
	nme_jni_get_member= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_get_member"),(int)2);
	nme_jni_set_member= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_set_member"),(int)3);
}

} // end namespace openfl
} // end namespace utils
