#include <hxcpp.h>

#ifndef INCLUDED_flash_Lib
#include <flash/Lib.h>
#endif
#ifndef INCLUDED_openfl_utils_JNIStaticField
#include <openfl/utils/JNIStaticField.h>
#endif
namespace openfl{
namespace utils{

Void JNIStaticField_obj::__construct(Dynamic field)
{
HX_STACK_PUSH("JNIStaticField::new","openfl/utils/JNI.hx",156);
{
	HX_STACK_LINE(156)
	this->field = field;
}
;
	return null();
}

JNIStaticField_obj::~JNIStaticField_obj() { }

Dynamic JNIStaticField_obj::__CreateEmpty() { return  new JNIStaticField_obj; }
hx::ObjectPtr< JNIStaticField_obj > JNIStaticField_obj::__new(Dynamic field)
{  hx::ObjectPtr< JNIStaticField_obj > result = new JNIStaticField_obj();
	result->__construct(field);
	return result;}

Dynamic JNIStaticField_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JNIStaticField_obj > result = new JNIStaticField_obj();
	result->__construct(inArgs[0]);
	return result;}

Dynamic JNIStaticField_obj::set( Dynamic value){
	HX_STACK_PUSH("JNIStaticField::set","openfl/utils/JNI.hx",170);
	HX_STACK_THIS(this);
	HX_STACK_ARG(value,"value");
	HX_STACK_LINE(172)
	::openfl::utils::JNIStaticField_obj::nme_jni_set_static(this->field,value);
	HX_STACK_LINE(173)
	return value;
}


HX_DEFINE_DYNAMIC_FUNC1(JNIStaticField_obj,set,return )

Dynamic JNIStaticField_obj::get( ){
	HX_STACK_PUSH("JNIStaticField::get","openfl/utils/JNI.hx",163);
	HX_STACK_THIS(this);
	HX_STACK_LINE(163)
	return ::openfl::utils::JNIStaticField_obj::nme_jni_get_static(this->field);
}


HX_DEFINE_DYNAMIC_FUNC0(JNIStaticField_obj,get,return )

Dynamic JNIStaticField_obj::nme_jni_get_static;

Dynamic JNIStaticField_obj::nme_jni_set_static;


JNIStaticField_obj::JNIStaticField_obj()
{
}

void JNIStaticField_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(JNIStaticField);
	HX_MARK_MEMBER_NAME(field,"field");
	HX_MARK_END_CLASS();
}

void JNIStaticField_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(field,"field");
}

Dynamic JNIStaticField_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"set") ) { return set_dyn(); }
		if (HX_FIELD_EQ(inName,"get") ) { return get_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"field") ) { return field; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"nme_jni_get_static") ) { return nme_jni_get_static; }
		if (HX_FIELD_EQ(inName,"nme_jni_set_static") ) { return nme_jni_set_static; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JNIStaticField_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"field") ) { field=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"nme_jni_get_static") ) { nme_jni_get_static=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"nme_jni_set_static") ) { nme_jni_set_static=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JNIStaticField_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("field"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("nme_jni_get_static"),
	HX_CSTRING("nme_jni_set_static"),
	String(null()) };

static ::String sMemberFields[] = {
	HX_CSTRING("set"),
	HX_CSTRING("get"),
	HX_CSTRING("field"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JNIStaticField_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(JNIStaticField_obj::nme_jni_get_static,"nme_jni_get_static");
	HX_MARK_MEMBER_NAME(JNIStaticField_obj::nme_jni_set_static,"nme_jni_set_static");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JNIStaticField_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(JNIStaticField_obj::nme_jni_get_static,"nme_jni_get_static");
	HX_VISIT_MEMBER_NAME(JNIStaticField_obj::nme_jni_set_static,"nme_jni_set_static");
};

Class JNIStaticField_obj::__mClass;

void JNIStaticField_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("openfl.utils.JNIStaticField"), hx::TCanCast< JNIStaticField_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void JNIStaticField_obj::__boot()
{
	nme_jni_get_static= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_get_static"),(int)1);
	nme_jni_set_static= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_set_static"),(int)2);
}

} // end namespace openfl
} // end namespace utils
