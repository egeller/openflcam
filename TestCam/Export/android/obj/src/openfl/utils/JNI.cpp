#include <hxcpp.h>

#ifndef INCLUDED_Reflect
#include <Reflect.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flash_Lib
#include <flash/Lib.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_openfl_utils_JNI
#include <openfl/utils/JNI.h>
#endif
#ifndef INCLUDED_openfl_utils_JNIMemberField
#include <openfl/utils/JNIMemberField.h>
#endif
#ifndef INCLUDED_openfl_utils_JNIMethod
#include <openfl/utils/JNIMethod.h>
#endif
#ifndef INCLUDED_openfl_utils_JNIStaticField
#include <openfl/utils/JNIStaticField.h>
#endif
namespace openfl{
namespace utils{

Void JNI_obj::__construct()
{
	return null();
}

JNI_obj::~JNI_obj() { }

Dynamic JNI_obj::__CreateEmpty() { return  new JNI_obj; }
hx::ObjectPtr< JNI_obj > JNI_obj::__new()
{  hx::ObjectPtr< JNI_obj > result = new JNI_obj();
	result->__construct();
	return result;}

Dynamic JNI_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< JNI_obj > result = new JNI_obj();
	result->__construct();
	return result;}

bool JNI_obj::initialized;

Void JNI_obj::init( ){
{
		HX_STACK_PUSH("JNI::init","openfl/utils/JNI.hx",17);
		HX_STACK_LINE(17)
		if ((!(::openfl::utils::JNI_obj::initialized))){
			HX_STACK_LINE(21)
			::openfl::utils::JNI_obj::initialized = true;
			HX_STACK_LINE(22)
			Dynamic method = ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_init_callback"),(int)1);		HX_STACK_VAR(method,"method");
			HX_STACK_LINE(23)
			method(::openfl::utils::JNI_obj::onCallback_dyn());
		}
	}
return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(JNI_obj,init,(void))

Dynamic JNI_obj::onCallback( Dynamic object,Dynamic method,Dynamic args){
	HX_STACK_PUSH("JNI::onCallback","openfl/utils/JNI.hx",30);
	HX_STACK_ARG(object,"object");
	HX_STACK_ARG(method,"method");
	HX_STACK_ARG(args,"args");
	HX_STACK_LINE(32)
	Dynamic field = ::Reflect_obj::field(object,method);		HX_STACK_VAR(field,"field");
	HX_STACK_LINE(34)
	if (((field != null()))){
		HX_STACK_LINE(34)
		return ::Reflect_obj::callMethod(object,field,args);
	}
	HX_STACK_LINE(40)
	::haxe::Log_obj::trace((HX_CSTRING("onCallback - unknown field ") + ::Std_obj::string(method)),hx::SourceInfo(HX_CSTRING("JNI.hx"),40,HX_CSTRING("openfl.utils.JNI"),HX_CSTRING("onCallback")));
	HX_STACK_LINE(41)
	return null();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(JNI_obj,onCallback,return )

::openfl::utils::JNIMemberField JNI_obj::createMemberField( ::String className,::String memberName,::String signature){
	HX_STACK_PUSH("JNI::createMemberField","openfl/utils/JNI.hx",46);
	HX_STACK_ARG(className,"className");
	HX_STACK_ARG(memberName,"memberName");
	HX_STACK_ARG(signature,"signature");
	HX_STACK_LINE(48)
	::openfl::utils::JNI_obj::init();
	HX_STACK_LINE(50)
	return ::openfl::utils::JNIMemberField_obj::__new(::openfl::utils::JNI_obj::nme_jni_create_field(className,memberName,signature,false));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(JNI_obj,createMemberField,return )

Dynamic JNI_obj::createMemberMethod( ::String className,::String memberName,::String signature,hx::Null< bool >  __o_useArray){
bool useArray = __o_useArray.Default(false);
	HX_STACK_PUSH("JNI::createMemberMethod","openfl/utils/JNI.hx",55);
	HX_STACK_ARG(className,"className");
	HX_STACK_ARG(memberName,"memberName");
	HX_STACK_ARG(signature,"signature");
	HX_STACK_ARG(useArray,"useArray");
{
		HX_STACK_LINE(57)
		::openfl::utils::JNI_obj::init();
		HX_STACK_LINE(59)
		::openfl::utils::JNIMethod method = ::openfl::utils::JNIMethod_obj::__new(::openfl::utils::JNI_obj::nme_jni_create_method(className,memberName,signature,false));		HX_STACK_VAR(method,"method");
		HX_STACK_LINE(60)
		return method->getMemberMethod(useArray);
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC4(JNI_obj,createMemberMethod,return )

::openfl::utils::JNIStaticField JNI_obj::createStaticField( ::String className,::String memberName,::String signature){
	HX_STACK_PUSH("JNI::createStaticField","openfl/utils/JNI.hx",65);
	HX_STACK_ARG(className,"className");
	HX_STACK_ARG(memberName,"memberName");
	HX_STACK_ARG(signature,"signature");
	HX_STACK_LINE(67)
	::openfl::utils::JNI_obj::init();
	HX_STACK_LINE(69)
	return ::openfl::utils::JNIStaticField_obj::__new(::openfl::utils::JNI_obj::nme_jni_create_field(className,memberName,signature,true));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(JNI_obj,createStaticField,return )

Dynamic JNI_obj::createStaticMethod( ::String className,::String memberName,::String signature,hx::Null< bool >  __o_useArray){
bool useArray = __o_useArray.Default(false);
	HX_STACK_PUSH("JNI::createStaticMethod","openfl/utils/JNI.hx",74);
	HX_STACK_ARG(className,"className");
	HX_STACK_ARG(memberName,"memberName");
	HX_STACK_ARG(signature,"signature");
	HX_STACK_ARG(useArray,"useArray");
{
		HX_STACK_LINE(76)
		::openfl::utils::JNI_obj::init();
		HX_STACK_LINE(78)
		::openfl::utils::JNIMethod method = ::openfl::utils::JNIMethod_obj::__new(::openfl::utils::JNI_obj::nme_jni_create_method(className,memberName,signature,true));		HX_STACK_VAR(method,"method");
		HX_STACK_LINE(79)
		return method->getStaticMethod(useArray);
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC4(JNI_obj,createStaticMethod,return )

Dynamic JNI_obj::getEnv( ){
	HX_STACK_PUSH("JNI::getEnv","openfl/utils/JNI.hx",84);
	HX_STACK_LINE(86)
	::openfl::utils::JNI_obj::init();
	HX_STACK_LINE(88)
	return ::openfl::utils::JNI_obj::nme_jni_get_env();
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(JNI_obj,getEnv,return )

Dynamic JNI_obj::nme_jni_create_field;

Dynamic JNI_obj::nme_jni_create_method;

Dynamic JNI_obj::nme_jni_get_env;


JNI_obj::JNI_obj()
{
}

void JNI_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(JNI);
	HX_MARK_END_CLASS();
}

void JNI_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic JNI_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"init") ) { return init_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"getEnv") ) { return getEnv_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"onCallback") ) { return onCallback_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { return initialized; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"nme_jni_get_env") ) { return nme_jni_get_env; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"createMemberField") ) { return createMemberField_dyn(); }
		if (HX_FIELD_EQ(inName,"createStaticField") ) { return createStaticField_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"createMemberMethod") ) { return createMemberMethod_dyn(); }
		if (HX_FIELD_EQ(inName,"createStaticMethod") ) { return createStaticMethod_dyn(); }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"nme_jni_create_field") ) { return nme_jni_create_field; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"nme_jni_create_method") ) { return nme_jni_create_method; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic JNI_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 11:
		if (HX_FIELD_EQ(inName,"initialized") ) { initialized=inValue.Cast< bool >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"nme_jni_get_env") ) { nme_jni_get_env=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 20:
		if (HX_FIELD_EQ(inName,"nme_jni_create_field") ) { nme_jni_create_field=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"nme_jni_create_method") ) { nme_jni_create_method=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void JNI_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("initialized"),
	HX_CSTRING("init"),
	HX_CSTRING("onCallback"),
	HX_CSTRING("createMemberField"),
	HX_CSTRING("createMemberMethod"),
	HX_CSTRING("createStaticField"),
	HX_CSTRING("createStaticMethod"),
	HX_CSTRING("getEnv"),
	HX_CSTRING("nme_jni_create_field"),
	HX_CSTRING("nme_jni_create_method"),
	HX_CSTRING("nme_jni_get_env"),
	String(null()) };

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(JNI_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(JNI_obj::initialized,"initialized");
	HX_MARK_MEMBER_NAME(JNI_obj::nme_jni_create_field,"nme_jni_create_field");
	HX_MARK_MEMBER_NAME(JNI_obj::nme_jni_create_method,"nme_jni_create_method");
	HX_MARK_MEMBER_NAME(JNI_obj::nme_jni_get_env,"nme_jni_get_env");
};

static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(JNI_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(JNI_obj::initialized,"initialized");
	HX_VISIT_MEMBER_NAME(JNI_obj::nme_jni_create_field,"nme_jni_create_field");
	HX_VISIT_MEMBER_NAME(JNI_obj::nme_jni_create_method,"nme_jni_create_method");
	HX_VISIT_MEMBER_NAME(JNI_obj::nme_jni_get_env,"nme_jni_get_env");
};

Class JNI_obj::__mClass;

void JNI_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("openfl.utils.JNI"), hx::TCanCast< JNI_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics, sVisitStatics);
}

void JNI_obj::__boot()
{
	initialized= false;
	nme_jni_create_field= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_create_field"),(int)4);
	nme_jni_create_method= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_create_method"),(int)4);
	nme_jni_get_env= ::flash::Lib_obj::load(HX_CSTRING("nme"),HX_CSTRING("nme_jni_get_env"),(int)0);
}

} // end namespace openfl
} // end namespace utils
