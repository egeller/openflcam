package testcam;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.os.Environment;
//import org.haxe.nme.GameActivity;
import testcam.CamActivity;
import java.io.File;
import android.net.Uri;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


class TestCam{
  private static Uri mUri;
  private static final String TAG = "TestCamExt";
  public static void startTake(String in){
    
    final String folder = in;
    final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
  //     startActivityForResult(takePictureIntent, 100);
       	CamActivity.getInstance( ).runOnUiThread(
      new Runnable( ) {
      public void run() {
/*      final File directory = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), folder);
        
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e(TAG, "Failed to create storage directory.");
      //          return null;
            }
        } 
        
        */  
    File directory = new File(Environment.getExternalStorageDirectory() + "/" + folder + "/");
                // have the object build the directory structure, if needed.
    directory.mkdirs();    
           
    final String timeStamp = new SimpleDateFormat("yyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
	  final File f = new File(directory.getPath() + File.separator + "IMG_"
                + timeStamp + ".jpg");
      takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
      mUri = Uri.fromFile(f);
      CamActivity.getInstance( ).startActivityForResult(takePictureIntent, 100);
	  
      }
	  
//	  String uristr = mUri.toString();
//	  return uristr;
      });


  
  }
  
  
  
 
    public static String doSomething(String in){
        
        startTake(in);
		final String uristr = mUri.toString();
//		GameActivity.getInstance().getActivityForResult
		return uristr;
    }
}