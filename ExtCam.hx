package;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end


class ExtCam {
	
	
	public static function sampleMethod (inputValue:Int):Int {
		
		return extcam_sample_method(inputValue);
		
	}
	
	
	private static var extcam_sample_method = Lib.load ("extcam", "extcam_sample_method", 1);
	
	#if android
// К сожалению, мы не можем использовать функции JNI до выполнения функции main.
// Я пробовал, верьте мне :)
private static var testcam_dosomething : Dynamic;

private static function init(){
    if (testcam_dosomething != null)
        return;
    testcam_dosomething = openfl.utils.JNI.createStaticMethod(
        "testcam/TestCam",
        "doSomething",
        "(Ljava/lang/String;)Ljava/lang/String;"
    );
}
public static function doSomething(str:String) : String {
    init();
    return testcam_dosomething(str);
}
#end
	
}